import numpy as np
import cv2 as cv
from matplotlib import pyplot as plt
import math

MIN_MATCH_COUNT = 6
THRESHOLD=1000
num=0

def jaccard(i, boxB):
	# determine the (x, y)-coordinates of the intersection rectangle
	global num
	if 'NaN' in gts[i-1]:
		return 2
	num+=1
	boxA=[int(float(x)) for x in gts[i-1].split(',')]
	xA = max(boxA[0], boxB[0])
	yA = max(boxA[1], boxB[1])
	xB = min(boxA[2], boxB[2])
	yB = min(boxA[3], boxB[3])
 
	# compute the area of intersection rectangle
	interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)
 
	# compute the area of both the prediction and ground-truth
	# rectangles
	boxAArea = (boxA[2] - boxA[0] + 1) * (boxA[3] - boxA[1] + 1)
	boxBArea = (boxB[2] - boxB[0] + 1) * (boxB[3] - boxB[1] + 1)
 
	# compute the intersection over union by taking the intersection
	# area and dividing it by the sum of prediction + ground-truth
	# areas - the interesection area
	iou = interArea / float(boxAArea + boxBArea - interArea)
 
	# return the intersection over union value
	return iou

def getReference(i):
	global img1, x, x1, y, y1, err
	img1=cv.imread('../car2/%05d.jpg'%(i),0)
	if "NaN" in gts[i-1]:
		print("No ground truth!")			
		y_img=int(min(y, y1))
		h_img=int(abs(y-y1))
		x_img=int(min(x, x1))
		w_img=int(abs(x-x1))
		img1=img1[y_img:y_img+h_img,x_img:x_img+w_img]
			
	else:
		pos = gts[i-1].split(',')
		pos = [int(float(x)) for x in pos]
		xs=[pos[0], pos[2]]
		x=xs[0]
		x1=xs[1]
		ys=[pos[1], pos[3]]
		y=ys[0]
		y1=ys[1]
		img1=img1[max(ys[0],0):ys[1], max(xs[0],0):xs[1]]
	#cv.imshow("Referência", img1)
	#cv.waitKey(1000)

img1=None
gts=None
err=0
x=0
x1=0
y=0
y1=0
media=0
acc_jac=0
with open("../gtcar2.txt","r") as f:
	text = f.read()
	gts = text.split('\n') 



getReference(1)
# Initiate SIFT detector
sift = cv.xfeatures2d.SIFT_create()
# find the keypoints and descriptors with SIFT
FLANN_INDEX_KDTREE = 1
index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 3, table_number=12)
search_params = dict(checks = 25)
flann = cv.FlannBasedMatcher(index_params, search_params)
for i in range(1,946):
	kp1, des1 = sift.detectAndCompute(img1,None)
	img2 = cv.imread('../car2/%05d.jpg'%i,0) 		# trainImage
	kp2, des2 = sift.detectAndCompute(img2,None)
	if(len(kp1)>=2 and len(kp2)<=2): 
		continue	
	matches = flann.knnMatch(des1,des2,k=2)
	# store all the good matches as per Lowe's ratio test.
	good = []
	for m,n in matches:
		if m.distance < 0.7*n.distance:
			good.append(m)
	if len(good)>MIN_MATCH_COUNT:
		media=0
		src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1,1,2)		
		dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1,1,2)
		M, mask = cv.findHomography(src_pts, dst_pts, cv.RANSAC,5.0)
		matchesMask = mask.ravel().tolist()
		h,w = img1.shape
		pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ]).reshape(-1,1,2)
		dst = cv.perspectiveTransform(pts,M)
		x = dst[0,0,0] if abs(x-dst[0,0,0])<THRESHOLD else (0.3*x+1.7*dst[0,0,0])/2
		y = dst[0,0,1] if abs(y-dst[0,0,1])<THRESHOLD else (0.3*y+1.7*dst[0,0,1])/2
		x1 = dst[2,0,0] if abs(x1-dst[2,0,0])<THRESHOLD else (0.3*x1+1.7*dst[2,0,0])/2
		y1 = dst[2,0,1] if abs(y1-dst[2,0,1])<THRESHOLD else (0.3*y1+1.7*dst[2,0,1])/2
	elif len(good)>4:
		dst_des = np.float32([des2[m.trainIdx] for m in good ])
		img3=cv.imread('../car2/%05d.jpg'%(i-1),0)
		kp3, des3 = sift.detectAndCompute(img3,None)
		matches = flann.knnMatch(des1,des3,k=2)
		good = []
		for m,n in matches:
			if m.distance < 0.7*n.distance:
				good.append(m)
		dst_pts=[0]
		src_pts=[0]
		if len(good)>3:
			src_des = np.float32([des3[m.trainIdx] for m in good ])
			matches = flann.knnMatch(dst_des,src_des,k=2)
			good = []
			for m,n in matches:
				if m.distance < 0.7*n.distance:
					good.append(m)
			dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ])
			src_pts = np.float32([ kp3[m.queryIdx].pt for m in good ])
			if len(dst_pts) > 8 and len(src_pts) > 8: 
				dx_mean=(dst_pts[0][0]-src_pts[0][0])
				dy_mean=(dst_pts[0][1]-src_pts[0][1])
				for i in range(min(len(dst_pts), len(src_pts))):
					dx_mean=(dx_mean+(dst_pts[i,0]-src_pts[i,0]))/2
					dy_mean=(dy_mean+(dst_pts[i,1]-src_pts[i,1]))/2
				if(abs(dx_mean) < abs(x-x1)):			
					x=0.7*x+0.3*(x+dx_mean)
					x1=0.7*x1+0.3*(x1+dx_mean)
				if(abs(dy_mean) < abs(y-y1)):
					y=0.7*y+0.3*(y+dy_mean)
					y1=0.7*y1+0.3*(y1+dy_mean)
	jac = jaccard(i, [x,y,x1,y1])
	if not jac > 0:
		err+=1
		getReference(i)
	if not jac==2:
		acc_jac+=jac
	img3=cv.rectangle(img2, (int(x),int(y)), (int(x1), int(y1)), [(0,255,0),(255,255,255)][media>0], 2)
	cv.imshow("match",img2)
	cv.waitKey(1)

print("Taxa de falhas:\t%.04f\nJaccarde médio:\t%.02f\nTaxa de Robustez:\t%.02f"%(err/num, acc_jac/num, math.exp(-30*(err/num))))

