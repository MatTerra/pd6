#### Executando o código
Para utilizar o Tracking, execute o código com o seguinte comando.

```python PD6_Optimal<ID><CAR>.py```

Remova <ID> para executar o método apenas com descritores SIFT
Substitua <ID> por Average para executar o método de descritores e médias
Remova <CAR> para executar no vídeo 1
Substitua <CAR> por Car2 para executar no vídeo 2

